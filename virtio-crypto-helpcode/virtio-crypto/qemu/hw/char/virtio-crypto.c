/*
 * Virtio Crypto Device
 *
 * Implementation of virtio-crypto qemu backend device.
 *
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr> 
 *
 */

#include <qemu/iov.h>
#include "hw/virtio/virtio-serial.h"
#include "hw/virtio/virtio-crypto.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <crypto/cryptodev.h>

static uint32_t get_features(VirtIODevice *vdev, uint32_t features)
{
	DEBUG_IN();
	return features;
}

static void get_config(VirtIODevice *vdev, uint8_t *config_data)
{
	DEBUG_IN();
}

static void set_config(VirtIODevice *vdev, const uint8_t *config_data)
{
	DEBUG_IN();
}

static void set_status(VirtIODevice *vdev, uint8_t status)
{
	DEBUG_IN();
}

static void vser_reset(VirtIODevice *vdev)
{
	DEBUG_IN();
}

static void vq_handle_output(VirtIODevice *vdev, VirtQueue *vq)
//Η συνάρτηση vq_handle_output είναι η συνάρτηση που κα-
//λείται όταν ο frontend οδηγός έχει προσθέσει ένα buffer στην ουρά.
{
	VirtQueueElement elem;
	unsigned int *syscall_type, *host_fd, *close_ret, cmd;

	DEBUG_IN();

	if (!virtqueue_pop(vq, &elem)) {
		DEBUG("No item to pop from VQ :(");
		return;
	} 

	DEBUG("I have got an item from VQ :)");

	syscall_type = elem.out_sg[0].iov_base; /////??????
	switch (*syscall_type) {
	//giati mas noiazei open, close? otan exoume etoimo buff dent ha einai mono gia ioctl? 
	//ennoei na anoiksoume session ;h syskeyh crypto?
//SYSKEYH CRYPTO
	case VIRTIO_CRYPTO_SYSCALL_TYPE_OPEN: //sg[2], systype out, host_fd in
		DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_OPEN");
		/* ?? */
		*host_fd = open("/dev/crypto", O_RDWR);
		unsigned char *input_msg = elem.in_sg[0].iov_base;
		memcpy(input_msg, *host_fd, sizeof(host_fd));

		break;

	case VIRTIO_CRYPTO_SYSCALL_TYPE_CLOSE: //sg[3], systype out, host_fd out, close ret in
		DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_CLOSE");
		/* ?? */
		host_fd = elem.out_sg[1].iov_base;
		*close_ret = close(*host_fd);
		unsigned char *input_msg = elem.in_sg[0].iov_base;
		memcpy(input_msg, *close_ret, sizeof(close_ret));

		break;

	case VIRTIO_CRYPTO_SYSCALL_TYPE_IOCTL: //sg[5], systype, fd, out(arg-> session or crypt-op),  cmd, in
		DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_IOCTL");
		/* ?? */
		host_fd = elem.out_sg[1].iov_base;
		unsigned long int *arg = elem.out_sg[2].iov_base;
		unsigned int *cmd = elem.out_sg[3].iov_base;

		int ret = ioctl(*host_fd, *cmd, *arg);

		unsigned char *input_msg = elem.in_sg[0].iov_base;
		memcpy(input_msg, ret, sizeof(ret));
		//printf("Guest says: %s\n", output_msg);
		//printf("We say: %s\n", input_msg);

		break;

	default:
		DEBUG("Unknown syscall_type");
	}

	virtqueue_push(vq, &elem, 0);
	virtio_notify(vdev, vq);
}

//arxikopoihsh odhgou.QQQ: to oti eexi anagnwristei h syskeyh (otan trexw to ./utopia ...) 
//shmainei oti exei ektelestei ayth edw katw (kai ara exei arxikopoih8ei kai to virtqueue)?
static void virtio_crypto_realize(DeviceState *dev, Error **errp)
{
    VirtIODevice *vdev = VIRTIO_DEVICE(dev);

	DEBUG_IN();

    virtio_init(vdev, "virtio-crypto", 13, 0); 
    //δημιουργεί μία virtio συσκευη
	virtio_add_queue(vdev, 128, vq_handle_output);
	//δημιουργεί μία virtqueue και την προσθέτει στη λίστα με τις virtqueues της συσκευής vdev .
}

static void virtio_crypto_unrealize(DeviceState *dev, Error **errp)
{
	DEBUG_IN();
}

static Property virtio_crypto_properties[] = {
    DEFINE_PROP_END_OF_LIST(),
};

static void virtio_crypto_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    VirtioDeviceClass *k = VIRTIO_DEVICE_CLASS(klass);

	DEBUG_IN();
    dc->props = virtio_crypto_properties;
    set_bit(DEVICE_CATEGORY_INPUT, dc->categories);

    k->realize = virtio_crypto_realize;
    k->unrealize = virtio_crypto_unrealize;
    k->get_features = get_features;
    k->get_config = get_config;
    k->set_config = set_config;
    k->set_status = set_status;
    k->reset = vser_reset;
}

static const TypeInfo virtio_crypto_info = {
    .name          = TYPE_VIRTIO_CRYPTO,
    .parent        = TYPE_VIRTIO_DEVICE,
    .instance_size = sizeof(VirtCrypto),
    .class_init    = virtio_crypto_class_init,
};

static void virtio_crypto_register_types(void)
{
    type_register_static(&virtio_crypto_info);
}

type_init(virtio_crypto_register_types)
