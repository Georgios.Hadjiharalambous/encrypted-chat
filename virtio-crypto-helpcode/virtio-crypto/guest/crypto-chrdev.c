/*
 * crypto-chrdev.c
 *
 * Implementation of character devices
 * for virtio-crypto device 
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr>
 *
 */
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/wait.h>
#include <linux/virtio.h>
#include <linux/virtio_config.h>

#include "crypto.h"
#include "crypto-chrdev.h"
#include "debug.h"

#include "cryptodev.h"

#define DATA_SIZE 1024
/*
 * Global data
 */
struct cdev crypto_chrdev_cdev;

/**
 * Given the minor number of the inode return the crypto device 
 * that owns that number.
 **/
static struct crypto_device *get_crypto_dev_by_minor(unsigned int minor)
{
	struct crypto_device *crdev;
	unsigned long flags;

	debug("Entering");

	spin_lock_irqsave(&crdrvdata.lock, flags);
	list_for_each_entry(crdev, &crdrvdata.devs, list) {
		if (crdev->minor == minor)
			goto out;
	}
	crdev = NULL;

out:
	spin_unlock_irqrestore(&crdrvdata.lock, flags);

	debug("Leaving");
	return crdev;
}

/*************************************
 * Implementation of file operations
 * for the Crypto character device
 *************************************/

//logika oti kaname kai stn prohgoumeno odhgo
static int crypto_chrdev_open(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	unsigned int len;
	struct crypto_open_file *crof;
	struct crypto_device *crdev;
	unsigned int *syscall_type, num_in, num_out;
	int *host_fd;
	struct virtqueue *vq = NULL;
	struct scatterlist syscall_type_sg, host_fd_sg,
	                   *sgs[2];

	debug("Entering");

	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTO_SYSCALL_OPEN;
	host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
	*host_fd = -1;
	//host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);


	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto fail;

	/* Associate this open file with the relevant crypto device. */
	crdev = get_crypto_dev_by_minor(iminor(inode));//edw exei ektelestei h realize (backend) opote exoume vq
	if (!crdev) {
		debug("Could not find crypto device with %u minor", 
		      iminor(inode));
		ret = -ENODEV;
		goto fail;
	}

	crof = kzalloc(sizeof(*crof), GFP_KERNEL);
	if (!crof) {
		ret = -ENOMEM;
		goto fail;
	}
	crof->crdev = crdev;
	crof->host_fd = -1;
	filp->private_data = crof;

	vq = crof->crdev->vq;

	/**
	 * We need two sg lists, one for syscall_type and one to get the 
	 * file descriptor from the host.
	 **/
	/* ?? */

	num_out = 0;
	num_in = 0;

	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sgs[num_out++] = &syscall_type_sg;
	sg_init_one(&host_fd_sg, host_fd, sizeof(*host_fd));
	sgs[num_out + num_in++] = &host_fd_sg;

	//prpeei na steiloume ena buff gia na ebergopoih8ei h vq_handle_output? gia na ginei open?
	//(diladi na kalesoume virtqueue_add_sgs, alla poio vq tha valoume?) 
	//NAI kai kick. YPARXEI vq apo to init (peigrafh sto txt)

	/**
	 * Wait for the host to process our data.
	 **/
	//twra ;h apla kaloume get_buff ;h block kapws.
	/* ?? */

	err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(vq);
	while (virtqueue_get_buf(vq, &len) == NULL)
		/* do nothing */;
	//theoume ayto ;h allou eidous block?

	ret = *host_fd;	
	crof->host_fd = *host_fd;
	if (*host_fd < 0) {
		ret = -ENODEV;
		goto fail;
	}
	/* If host failed to open() return -ENODEV. */
	/* ?? */

fail:	
	kfree(syscall_type);
	kfree(host_fd);

	debug("Leaving");
	return ret;
}

static int crypto_chrdev_release(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	struct virtqueue *vq = crof->crdev->vq;
	unsigned int *syscall_type, num_in, num_out, len; 
	int *host_fd, *close_ret;
	struct scatterlist syscall_type_sg, host_fd_sg, close_ret_sg,
	                   *sgs[3];

	debug("Entering");

	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTO_SYSCALL_CLOSE;
	host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
	*host_fd = crof->host_fd;
	close_ret = kzalloc(sizeof(*close_ret), GFP_KERNEL);
	*close_ret = 0;
	/**
	 * Send data to the host.
	 **/
	/* ?? */

	num_out = 0;
	num_in = 0;

	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sgs[num_out++] = &syscall_type_sg;
	sg_init_one(&host_fd_sg, host_fd, sizeof(*host_fd));
	sgs[num_out++] = &host_fd_sg;
	sg_init_one(&close_ret_sg, close_ret, sizeof(*close_ret));
	sgs[num_out + num_in++] = &close_ret_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */

	err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(vq);
	while (virtqueue_get_buf(vq, &len) == NULL)
		/* do nothing */;

	/*if (*close_ret < 0)
		ret = -1;

	kfree(crof);
	debug("Leaving");
	return ret;*/

	ret = *close_ret;

	kfree(syscall_type);
	kfree(host_fd);
	kfree(close_ret);
	kfree(crof);
	debug("Leaving");

	return ret;	//if close (host) is non zero (error), our release-close will return -1 as well

}

//giati ayta ta orismata gia tin ioctl kai oxi ta klassika?
static long crypto_chrdev_ioctl(struct file *filp, unsigned int cmd, 
                                unsigned long arg)
{
	long ret = 0;
	int err;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	struct virtqueue *vq = crdev->vq;
	struct scatterlist syscall_type_sg, output_msg_sg, input_msg_sg, cfd_sg, cmd_sg,
	                   *sgs[5]; //in,out buff, cfd, syscalltype, cmd
	unsigned int num_out, num_in, len;
	int *cfd;
#define MSG_LEN 100
	unsigned char /**output_msg,*/ *input_msg;
	unsigned long *output_msg;
	unsigned int *syscall_type;

	debug("Entering");

	/**
	 * Allocate all data that will be sent to the host.
	 **/
	//output_msg = kzalloc(MSG_LEN, GFP_KERNEL);
	output_msg = kzalloc(sizeof(arg), GFP_KERNEL);
	input_msg = kzalloc(DATA_SIZE, GFP_KERNEL);
	cfd = kzalloc(sizeof(*cfd), GFP_KERNEL);
	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTO_SYSCALL_IOCTL;

	num_out = 0;
	num_in = 0;

	*cfd = crof->host_fd;

	/**
	 *  These are common to all ioctl commands.
	 **/
	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sgs[num_out++] = &syscall_type_sg;
	sg_init_one(&cfd_sg, cfd, sizeof(*cfd));
	sgs[num_out++] = &cfd_sg;
	/* ?? */

//memcpy(output_msg, "Hello HOST from ioctl CIOCCRYPT.", 33);
	output_msg = &arg;
	input_msg[0] = '\0';
	sg_init_one(&output_msg_sg, output_msg, sizeof(arg));
	sgs[num_out++] = &output_msg_sg;
	sg_init_one(&cmd_sg, &cmd, sizeof(cmd));
	sgs[num_out++] = &cmd_sg;
	sg_init_one(&input_msg_sg, input_msg, DATA_SIZE);
	sgs[num_out + num_in++] = &input_msg_sg;

	/**
	 *  Add all the cmd specific sg lists.
	 **/
	/*
	switch (cmd) {
	case CIOCGSESSION:
		debug("CIOCGSESSION");
		//memcpy(output_msg, "Hello HOST from ioctl CIOCGSESSION.", 36);
		input_msg[0] = '\0';
		sg_init_one(&output_msg_sg, &arg, sizeof(arg)); //stelnoume apla th dieuthinsi tou struct
		//sg_init_one(&output_msg_sg, output_msg, MSG_LEN); //why scatterlists?/?????????????????????????
		sgs[num_out++] = &output_msg_sg;
		sg_init_one(&input_msg_sg, input_msg, DATA_SIZE);
		sgs[num_out + num_in++] = &input_msg_sg;

		break;
	
	case CIOCFSESSION:
		debug("CIOCFSESSION");
		//memcpy(output_msg, "Hello HOST from ioctl CIOCFSESSION.", 36);
		input_msg[0] = '\0';
		sg_init_one(&output_msg_sg, &arg, sizeof(arg));
		//sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		sg_init_one(&input_msg_sg, input_msg, DATA_SIZE);
		sgs[num_out + num_in++] = &input_msg_sg;

		break;

	case CIOCCRYPT:
		debug("CIOCCRYPT");
		//memcpy(output_msg, "Hello HOST from ioctl CIOCCRYPT.", 33);
		input_msg[0] = '\0';
		sg_init_one(&output_msg_sg, &arg, sizeof(arg));
		//sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		sg_init_one(&input_msg_sg, input_msg, DATA_SIZE);
		sgs[num_out + num_in++] = &input_msg_sg;

		break;

	default:
		debug("Unsupported ioctl command");

		break;
	}
	*/


	/**
	 * Wait for the host to process our data.
	 **/
	//giati edw? to wait den einai ousiastika sto while, afou exw perasei ta dedomena sth vq?
	/* ?? */
	/* ?? Lock ?? */   //?????
	err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(vq);
	while (virtqueue_get_buf(vq, &len) == NULL)
		/* do nothing */;
	//theoume ayto ;h allou eidous block?

	debug("We said: '%lu'", *output_msg);
	debug("Host answered: '%s'", input_msg);

	kfree(output_msg);
	kfree(input_msg);
	kfree(syscall_type);
	kfree(cfd);

	debug("Leaving");

	return ret;
}

static ssize_t crypto_chrdev_read(struct file *filp, char __user *usrbuf, 
                                  size_t cnt, loff_t *f_pos)
{
	debug("Entering");
	debug("Leaving");
	return -EINVAL;
}

static struct file_operations crypto_chrdev_fops = 
{
	.owner          = THIS_MODULE,
	.open           = crypto_chrdev_open,
	.release        = crypto_chrdev_release,
	.read           = crypto_chrdev_read,
	.unlocked_ioctl = crypto_chrdev_ioctl,
};

int crypto_chrdev_init(void)
{
	int ret;
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;
	
	debug("Initializing character device...");
	cdev_init(&crypto_chrdev_cdev, &crypto_chrdev_fops);
	crypto_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	ret = register_chrdev_region(dev_no, crypto_minor_cnt, "crypto_devs");
	if (ret < 0) {
		debug("failed to register region, ret = %d", ret);
		goto out;
	}
	ret = cdev_add(&crypto_chrdev_cdev, dev_no, crypto_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device");
		goto out_with_chrdev_region;
	}

	debug("Completed successfully");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
out:
	return ret;
}

void crypto_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

	debug("entering");
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	cdev_del(&crypto_chrdev_cdev);
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
	debug("leaving");
}
