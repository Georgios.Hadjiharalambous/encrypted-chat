/*
 * Implementation of character devices
 * for virtio-crypto device 
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr>
 *
 */

#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/wait.h>
#include <linux/virtio.h>
#include <linux/virtio_config.h>

#include "crypto.h"
#include "crypto-chrdev.h"
#include "debug.h"

#include "cryptodev.h"

#define DATA_SIZE 1024
/*
 * Global data
 */
struct cdev crypto_chrdev_cdev;

/**
 * Given the minor number of the inode return the crypto device 
 * that owns that number.
 **/
static struct crypto_device *get_crypto_dev_by_minor(unsigned int minor)
{
    struct crypto_device *crdev;
    unsigned long flags;

    debug("Entering");

    spin_lock_irqsave(&crdrvdata.lock, flags);
    list_for_each_entry(crdev, &crdrvdata.devs, list) {
        if (crdev->minor == minor)
            goto out;
    }
    crdev = NULL;

out:
    spin_unlock_irqrestore(&crdrvdata.lock, flags);

    debug("Leaving");
    return crdev; 
}

/*************************************
 * Implementation of file operations 
 * for the Crypto character device
 *************************************/ 
 static int crypto_chrdev_open(struct inode *inode, struct file *filp)
{
    int ret, err, *host_fd;
    struct crypto_open_file *crof;
    struct crypto_device *crdev;
    unsigned int *syscall_type, num_in, num_out, len;
    struct virtqueue *vq = NULL;
    struct scatterlist syscall_type_sg, host_fd_sg,
                       *sgs[2]; //sgs[2]: syscall_type (R), host_fd (W).

    debug("Entering open");
    syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
    *syscall_type = VIRTIO_CRYPTO_SYSCALL_OPEN;
    host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
    *host_fd = -1;
    ret = -ENODEV;

    if ((ret = nonseekable_open(inode, filp)) < 0)
        goto fail;

    /* Associate this open file with the relevant crypto device. */
    crdev = get_crypto_dev_by_minor(iminor(inode));

    if (!crdev) {
        debug("Could not find crypto device with %u minor",
              iminor(inode));
        ret = -ENODEV;
        goto fail;
    }

    crof = kzalloc(sizeof(*crof), GFP_KERNEL);
    if (!crof) {
        ret = -ENOMEM;
        goto fail;
    }

    crof->crdev = crdev;
    crof->host_fd = -1;
    filp->private_data = crof;

    vq = crof->crdev->vq; //The proper vq has been initialized (in crypto-module.c I think)

    /**
     * We need two sg lists, one for syscall_type and one to get the 
     * file descriptor from the host.
     **/
    num_out = 0;
    num_in = 0;

    sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
    sgs[num_out++] = &syscall_type_sg;
    sg_init_one(&host_fd_sg, host_fd, sizeof(*host_fd));
    sgs[num_out + num_in++] = &host_fd_sg;

    /**
     * Wait for the host to process our data.
     **/
    if(down_interruptible(&crdev->sem_lock)) { //Lock the semaphore of the crypto-device.
        debug("Unable to catch semaphore");
        return -ERESTARTSYS;
    }

    err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
                &syscall_type_sg, GFP_ATOMIC);
    virtqueue_kick(vq);

    //Busy wait because virtio_verify will cause virtual-interrupt, so get_buf
    //will run in interrupt-context.    
    while(virtqueue_get_buf(vq, &len) == NULL)
    	/* do nothing */;

    up(&crdev->sem_lock);

    if (*host_fd < 0) {
        ret = -ENODEV;
        goto fail;
    }
    else crof->host_fd = *host_fd;

    /* If host failed to open() return -ENODEV, else return 0. */

fail:
    kfree(syscall_type);
    kfree(host_fd);
    debug("Leaving open");
    return ret;
}




static int crypto_chrdev_release(struct inode *inode, struct file *filp)
{
    int ret = 0, err, *host_fd;
    struct crypto_open_file *crof = filp->private_data;
    struct crypto_device *crdev = crof->crdev;
    struct virtqueue *vq = crof->crdev->vq;
    unsigned int *syscall_type, num_in, num_out, len;
    struct scatterlist syscall_type_sg, host_fd_sg,
                       *sgs[2]; //sgs[2]: syscall_type (R), host_fd (R)

    debug("Entering close-realease");

    syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
    *syscall_type = VIRTIO_CRYPTO_SYSCALL_CLOSE;
    host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
    *host_fd = crof->host_fd;

    /**
     * Send data to the host.
     **/
    num_out = 0;
    num_in = 0;

    sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
    sgs[num_out++] = &syscall_type_sg;
    sg_init_one(&host_fd_sg, host_fd, sizeof(*host_fd));
    sgs[num_out++] = &host_fd_sg;

    /**
     * Wait for the host to process our data.
     **/
    if(down_interruptible(&crdev->sem_lock)) {
        debug("Unable to catch semaphore");
        return -ERESTARTSYS;
    }

    err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
                &syscall_type_sg, GFP_ATOMIC);
    virtqueue_kick(vq);

    while (virtqueue_get_buf(vq, &len) == NULL)
            /* do nothing */;

    up(&crdev->sem_lock);

    kfree(syscall_type);
    kfree(host_fd);
    kfree(crof);
    debug("Leaving close-release");
     return ret;     //Our close will always return 0...
}



static long crypto_chrdev_ioctl(struct file *filp, unsigned int cmd,
                                unsigned long arg)
{
    int ret = 0, err, *host_fd, *host_return_val, *ses_id;
    struct crypto_open_file *crof = filp->private_data;
    struct crypto_device *crdev = crof->crdev;
    struct virtqueue *vq = crdev->vq;
    struct scatterlist syscall_type_sg, output_msg_sg, input_msg_sg, host_fd_sg, cmd_sg,
                        key_sg, crypt_sg, iv_sg, dst_sg, src_sg, session_sg,
                        host_return_val_sg,ses_id_sg;
    struct session_op *session = NULL;
    struct crypt_op *crypt = NULL;
    unsigned int num_out, num_in, len, *syscall_type, *ioctl_cmd;
    unsigned char *key, *src, *iv, *dst;
    unsigned long arg_dst, arg_key;

    debug("Entering ioctl");

    /**
     * Allocate all data that will be sent to the host.
     **/
    ioctl_cmd = kzalloc(sizeof(*ioctl_cmd), GFP_KERNEL); //pointer in kernelspace needed, &cmd has no point.
    *ioctl_cmd = cmd;
    host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
    *host_fd = crof->host_fd;
    host_return_val = kzalloc(sizeof(*host_return_val), GFP_KERNEL);
    syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
    *syscall_type = VIRTIO_CRYPTO_SYSCALL_IOCTL;

    num_out = 0;
    num_in = 0;

    /**
     *  Add all the cmd specific sg lists.
     **/

    /**
     *  These are common to all ioctl commands.
     **/
    sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
    sg_init_one(&host_fd_sg, host_fd, sizeof(*host_fd));
    sg_init_one(&cmd_sg, ioctl_cmd, sizeof(*ioctl_cmd));
    sg_init_one(&host_return_val_sg, host_return_val, sizeof(*host_return_val));

    //This will be used in any of the following cases.It doesn't matter if 
    //some *sgs[i] will stay empty as long as num_in and num_out are correct.
    struct scatterlist *sgs[8];

    sgs[num_out++] = &syscall_type_sg;
    sgs[num_out++] = &host_fd_sg;
    sgs[num_out++] = &cmd_sg;

    switch (cmd) {
    case CIOCGSESSION:
        debug("CIOCGSESSION");

        //At this cmd-case, arg will be a pointer to a struct session_op.
        session = kzalloc(sizeof(struct session_op), GFP_KERNEL);
        if(copy_from_user(session, arg, sizeof(struct session_op))){
            debug("Error in copy_to_user-ciocgsession session");
            return -EFAULT;
        }

        //key is a pointer-field in session, so it must be sent in its own.
        key = kzalloc((session->keylen)*sizeof(char), GFP_KERNEL);
        if(copy_from_user(key, session->key, (session->keylen)*sizeof(char))){
            debug("Error in copy_to_user-ciocgsession key");
	        return -EFAULT;
        }

        sg_init_one(&key_sg, key, sizeof(*key));
        sgs[num_out++] = &key_sg;
        sg_init_one(&session_sg, session, sizeof(*session));
        sgs[num_out + num_in++] = &session_sg;

        break;

    case CIOCFSESSION:
        debug("CIOCFSESSION");

        //At this cmd-case, arg will be a pointer to session->ses_id.
        ses_id = kzalloc(sizeof(*ses_id), GFP_KERNEL);
        if(copy_from_user(ses_id, arg, sizeof(*ses_id))){
            debug("Error in copy_to_user-ciocfsession ses_id");
            return -EFAULT;
        }

        sg_init_one(&ses_id_sg,ses_id, sizeof(*ses_id));
        sgs[num_out++]=&ses_id_sg;

        break;

    case CIOCCRYPT:
        debug("CIOCCRYPT");

        //At this cmd-case, arg will be a pointer to a struct crypt_op.
        crypt = kzalloc(sizeof(struct crypt_op), GFP_KERNEL);
        if(copy_from_user(crypt, arg, sizeof(struct crypt_op))){
            debug("Error in copy_to_user-cioccrypt crypt");
            return -EFAULT;
        }

        //The followings are pointer-fileds to crypt so they must be sent along.

        //Copy the true (userspace) pointer where dst should be copied later
        arg_dst = crypt->dst;

        src = kzalloc((crypt->len)*sizeof(char), GFP_KERNEL);
        if(copy_from_user(src, crypt->src, (crypt->len)*sizeof(char))){
            debug("Error in copy_to_user-cioccrypt src");
            return -EFAULT;
        }

        iv = kzalloc(strlen(crypt->iv)*sizeof(char), GFP_KERNEL);
        if(copy_from_user(iv, crypt->iv, strlen(crypt->iv)*sizeof(char))){
            debug("Error in copy_from_user-cioccrypt iv");
            return -EFAULT;
        }

        dst = kzalloc((crypt->len)*sizeof(char), GFP_KERNEL);

        sg_init_one(&crypt_sg,crypt,sizeof(*crypt));
        sg_init_one(&dst_sg,dst,sizeof(*dst));
        sg_init_one(&iv_sg,iv,sizeof(*iv));
        sg_init_one(&src_sg,src,sizeof(char)*(crypt->len));
        sgs[num_out++]=&crypt_sg;
        sgs[num_out++]=&src_sg;
        sgs[num_out++]=&iv_sg;
        sgs[num_out+num_in++]=&dst_sg;

        break;

    default:
        debug("Unsupported ioctl command");

        break;
    }

    //This will happen in any cmd-case
    sgs[num_out + num_in++] = &host_return_val_sg;

    /**
     * Wait for the host to process our data.
     **/
    if(down_interruptible(&crdev->sem_lock)) {
        debug("Unable to catch semaphore");
        return -ERESTARTSYS;
    }

    err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
                            &syscall_type_sg, GFP_ATOMIC);
    virtqueue_kick(vq);
    while (virtqueue_get_buf(vq, &len) == NULL)
            /* do nothing */;

    up(&crdev->sem_lock);

    //Again switch, for copy_to_user the data needed and free the proper pointers.
    switch(cmd){
    case CIOCGSESSION:
        if(copy_to_user(arg_key, key, (session->keylen)*sizeof(char))) {
            debug("Error in copy_to_user KEY -ciocgsession");
            return -EFAULT;
        }
        session->key = arg_key;
        
        if(copy_to_user(arg,session,sizeof(struct session_op))){
            debug("Error in copy_to_user SESSION -ciocgsession");
            return -EFAULT;
        }

        kfree(key);
        kfree(session);

        break;

    case CIOCFSESSION:
        kfree(ses_id);

        break;

    case CIOCCRYPT:
        if(copy_to_user(arg_dst, dst, crypt->len)){
            debug("Error in copy_to_user-cioccrypt");
            return -EFAULT;
        }

        kfree(src);
        kfree(iv);
        kfree(dst);
        kfree(crypt);

        break;
    default:
        debug("Wrong ioctl command");
    }

    //Ioctl return val will be the return val of the real ioctl command (backend)
    ret = *host_return_val;

    kfree(host_return_val);
    kfree(syscall_type);
    kfree(ioctl_cmd);
    kfree(host_fd);

    debug("Leaving ioctl");

    return ret;
}



static ssize_t crypto_chrdev_read(struct file *filp, char __user *usrbuf, 
                                  size_t cnt, loff_t *f_pos)
{
	debug("Entering");
	debug("Leaving");
	return -EINVAL;
}

static struct file_operations crypto_chrdev_fops = 
{
	.owner          = THIS_MODULE,
	.open           = crypto_chrdev_open,
	.release        = crypto_chrdev_release,
	.read           = crypto_chrdev_read,
	.unlocked_ioctl = crypto_chrdev_ioctl,
};

int crypto_chrdev_init(void)
{
    int ret;
    dev_t dev_no;
    unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

    debug("Initializing character device...");
    cdev_init(&crypto_chrdev_cdev, &crypto_chrdev_fops);
    crypto_chrdev_cdev.owner = THIS_MODULE;

    dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
    ret = register_chrdev_region(dev_no, crypto_minor_cnt, "crypto_devs");
    if (ret < 0) {
        debug("failed to register region, ret = %d", ret);
        goto out;
    }
    ret = cdev_add(&crypto_chrdev_cdev, dev_no, crypto_minor_cnt);
    if (ret < 0) {
        debug("failed to add character device");
        goto out_with_chrdev_region;
    }

    debug("Completed successfully");
    return 0;

out_with_chrdev_region:
    unregister_chrdev_region(dev_no, crypto_minor_cnt);
out:
    return ret;
}

void crypto_chrdev_destroy(void)
{
    dev_t dev_no;
    unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

    debug("entering");
    dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
    cdev_del(&crypto_chrdev_cdev);
    unregister_chrdev_region(dev_no, crypto_minor_cnt);
    debug("leaving");
}


